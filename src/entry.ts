
/**
 * Include the app-specific stylesheets.
 * These will be extracted down to the valid .css files using extract-text-webpack-plugin.
 */
require('./stylesheet/app.scss');

import {provide, enableProdMode} from 'angular2/core';
import {bootstrap, ELEMENT_PROBE_PROVIDERS} from 'angular2/platform/browser';
import {ROUTER_PROVIDERS, APP_BASE_HREF} from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';
import {AppComponent} from './app/app.component';

const ENV_PROVIDERS = [];

/**
if (process.env.ENV) {
    enableProdMode();
} else {
    ENV_PROVIDERS.push(ELEMENT_PROBE_PROVIDERS);
}
*/

document.addEventListener('DOMContentLoaded', function() {
    bootstrap(AppComponent, [
        ENV_PROVIDERS,
        HTTP_PROVIDERS,
        ROUTER_PROVIDERS,
        provide(APP_BASE_HREF, { useValue: process.env.APP_BASE_HREF })
    ])
});