import {Component} from 'angular2/core';

@Component({
    selector: 'dashboard',
    template: require('./dashboard.html')
})
export class DashboardComponent
{
    public constructor()
    {
        console.log('constructing dashboard');
    }
}